//
//  ExtraMessage.swift
//  App
//
//  Created by vadim vitvickiy on 25/07/2018.
//

import Vapor
import FluentPostgreSQL
import Telegrammer

enum extraType: UInt8, Codable {
    case text
    case image
    case document
    case video
    case audio
    case sticker
    
    static var allCases: [extraType] {
        return [extraType.text, extraType.image, extraType.document, extraType.video, extraType.audio]
    }
}

struct Extra: Model, Migration {
    
    typealias Database = PostgreSQLDatabase
    
    typealias ID = Int64
    
    static let idKey: IDKey = \.id
    
    var id: ID?
    var chatID: Int64
    var type: extraType
    var hashtag: String
    var text: String?
    var mediaID: String?
    
    var chat: Parent<Extra, Chat> {
        return parent(\.chatID)
    }
    
    init(message: Message, hachetag: String) {
        
        var type: extraType
        var text: String?
        var mediaID: String?
        
        if let sticker = message.sticker {
            type = .sticker
            mediaID = sticker.fileId
        } else if let audio = message.audio {
            type = .audio
            mediaID = audio.fileId
        } else if let document = message.document {
            type = .document
            mediaID = document.fileId
        } else if let video = message.video {
            type = .video
            mediaID = video.fileId
        } else if let photos = message.photo {
            type = .image
            mediaID = photos.first?.fileId
        } else {
            type = .text
            text = message.text
        }
        
        self.chatID = message.chat.id
        self.hashtag = hachetag
        self.type = type
        self.text = text
        self.mediaID = mediaID
    }
}

extension Extra: Hashable {
    
    var hashValue: Int {
        return hashtag.hashValue
    }
    
    static func == (lhs: Extra, rhs: Extra) -> Bool {
        return lhs.hashtag == rhs.hashtag
    }
}
