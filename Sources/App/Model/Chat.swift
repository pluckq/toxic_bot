//
//  Chat.swift
//  App
//
//  Created by vadim vitvickiy on 29/07/2018.
//

import Vapor
import FluentPostgreSQL
import Telegrammer

struct Chat: Model, Migration {
    
    typealias Database = PostgreSQLDatabase
    
    typealias ID = Int64
    
    static let idKey: IDKey = \.id
    
    var id: ID?
    
    var extras: Children<Chat, Extra> {
        return children(\.chatID)
    }
    
    var users: Children<Chat, User> {
        return children(\.chatID)
    }
    
    init(id: Int64) {
        self.id = id
    }
}

