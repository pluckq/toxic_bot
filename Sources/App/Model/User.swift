//
//  User.swift
//  App
//
//  Created by vadim vitvickiy on 29/07/2018.
//

import Vapor
import FluentPostgreSQL
import Telegrammer

struct User: Model, Migration {

    typealias Database = PostgreSQLDatabase
    
    typealias ID = Int64
    
    static let idKey: IDKey = \.id
    
    var id: ID?
    var chatID: Int64
    
    var chat: Parent<User, Chat> {
        return parent(\.chatID)
    }
}

extension User: Hashable {
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
}
