//
//  Bot+SendMessage.swift
//  App
//
//  Created by vadim vitvickiy on 03/07/2018.
//

import Telegrammer

extension Bot {
    
    func sendMessage(chatID: Int64, text: String, replayMessage: Int?) {
        do {
            try sendMessage(params: Bot.SendMessageParams(chatId: ChatId.chat(chatID), text: text, replyToMessageId: replayMessage))
        } catch {
            print(error)
        }
    }
}
