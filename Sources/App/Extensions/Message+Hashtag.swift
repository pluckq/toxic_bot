//
//  Message+Hashtag.swift
//  App
//
//  Created by vadim vitvickiy on 31/07/2018.
//

import Telegrammer

extension Message {
    
    func findHashtag() -> String? {
        guard let text = text,
            let hachetagEntity = entities?.filter({$0.type.rawValue == "hashtag"}).first
            else { return nil }
        
        let startIndex = text.index(text.startIndex, offsetBy: hachetagEntity.offset)
        let endIndex = text.index(text.startIndex, offsetBy: hachetagEntity.offset + hachetagEntity.length)
        let hashtag = String(text[startIndex..<endIndex])
        
        return hashtag
    }
}
