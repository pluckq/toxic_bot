import Vapor
import Telegrammer

public func boot(_ app: Application) throws {
    let botService = try app.make(ToxicBot.self)
    try botService.updater?.startLongpolling().wait()
}
