import Vapor
import FluentPostgreSQL

var pool: DatabaseConnectionPool<PostgreSQLDatabase>!

public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    services = Services.default()
    
    let serverConfig = NIOServerConfig.default(hostname: "127.0.0.1")
    services.register(serverConfig)
    
    var middlewareConfig = MiddlewareConfig()
    middlewareConfig.use(ErrorMiddleware.self)
    middlewareConfig.use(SessionsMiddleware.self)
    services.register(middlewareConfig)
    
    let poolConfig = DatabaseConnectionPoolConfig(maxConnections: 8)
    services.register(poolConfig)
    
    try? services.register(FluentPostgreSQLProvider())
    
    let postgresqlConfig = PostgreSQLDatabaseConfig(
        hostname: "127.0.0.1",
        port: 5432,
        username: "vadimvitvickiy",
        database: "toxicbotdb",
        password: nil
    )
    services.register(postgresqlConfig)
    
    let db = PostgreSQLDatabase(config: postgresqlConfig)
    let requestWorker = MultiThreadedEventLoopGroup(numberOfThreads: 8)
    pool = db.newConnectionPool(config: poolConfig, on: requestWorker)
    var databases = DatabasesConfig()
    databases.add(database: db, as: .psql)
    services.register(databases)
    
    var migrations = MigrationConfig()
    migrations.add(model: Chat.self, database: .psql)
    Chat.defaultDatabase = .psql
    migrations.add(model: Extra.self, database: .psql)
    Extra.defaultDatabase = .psql
    migrations.add(model: User.self, database: .psql)
    User.defaultDatabase = .psql
    services.register(migrations)
    
    services.register(ToxicBot.self)
}
