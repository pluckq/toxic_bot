//
//  ToxicBot.swift
//  App
//
//  Created by vadim vitvickiy on 30/06/2018.
//

import Foundation
import Telegrammer
import Vapor

final class ToxicBot: ServiceType {
    
    let bot: Bot
    var updater: Updater?
    let rightsValidator: RightsValidator
    let errorHandler: ErrorHandler
    
    init(settings: Bot.Settings) throws {
        bot = try Bot(settings: settings)
        rightsValidator = RightsValidator(bot: bot)
        errorHandler = ErrorHandler(bot: bot)
        
        let dispatcher = configureDispatcher(bot: bot)
        updater = Updater(bot: bot, dispatcher: dispatcher)
    }
    
    func configureDispatcher(bot: Bot) -> Dispatcher {
        let dispatcher = Dispatcher(bot: bot)
        
        let promote = Promote(bot: bot, rightsValidator: rightsValidator)
        dispatcher.add(handler: promote.handler)
        
        let warn = Warn(bot: bot, rightsValidator: rightsValidator, errorHandler: errorHandler)
        dispatcher.add(handler: warn.handler)
        
        let warnCallback = WarnCallbackHandler()
        dispatcher.add(handler: warnCallback.handler)
        
        let kick = Kick(bot: bot, rightsValidator: rightsValidator, errorHandler: errorHandler)
        dispatcher.add(handler: kick.handler)
        
        let ban = Ban(bot: bot)
        dispatcher.add(handler: ban.handler)
        
        let vadik = Vadik(bot: bot, rightsValidator: rightsValidator, errorHandler: errorHandler)
        dispatcher.add(handler: vadik.handler)
        
        let extra = ExtraAdd(bot: bot)
        dispatcher.add(handler: extra.handler)
        
        let extraShow = ExtraShow(bot: bot)
        dispatcher.add(handler: extraShow.handler)
        
        let listener = Listener(bot: bot)
        dispatcher.add(handler: listener.handler)
        
        return dispatcher
    }

    static func makeService(for worker: Container) throws -> ToxicBot {
        guard let token = Environment.get("TELEGRAM_BOT_TOKEN") else {
            throw CoreError(identifier: "Enviroment variables", reason: "Cannot find telegram bot token")
        }

        let settings = Bot.Settings(token: token, debugMode: true)
        return try ToxicBot(settings: settings)
    }
}
