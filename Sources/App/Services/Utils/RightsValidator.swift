//
//  RightsChecker.swift
//  App
//
//  Created by vadim vitvickiy on 02/07/2018.
//

import Vapor
import Telegrammer

final class RightsValidator {
    
    typealias RequestData = (userID: Int64, chatID: Int64)
    
    private weak var bot: Bot?
    private var adminsId: Set<Int64>?
    private let requestWorker = MultiThreadedEventLoopGroup(numberOfThreads: 4)
    
    init(bot: Bot) {
        self.bot = bot
    }
    
    func admins(chatID: Int64) -> Future<Set<Int64>> {
        
        let promise = requestWorker.eventLoop.newPromise(Set<Int64>.self)
        
        requestWorker.eventLoop.execute { [weak self] in
            if let ids = self?.adminsId {
                promise.succeed(result: ids)
            } else {
                try? self?.bot?.getChatAdministrators(params: Bot.GetChatAdministratorsParams(chatId: ChatId.chat(chatID)))
                    .whenSuccess { admins in
                        let ids = Set(admins.map{$0.user.id})
                        self?.adminsId = ids
                        promise.succeed(result: ids)
                }
            }
        }
        
        return promise.futureResult
    }
}
