//
//  ErrorHandler.swift
//  App
//
//  Created by vadim vitvickiy on 23/07/2018.
//

import Telegrammer

class ErrorHandler {
    
    private weak var bot: Bot?
    
    init(bot: Bot) {
        self.bot = bot
    }
    
    @discardableResult
    func wrap<ReturnType>(chatID: Int64, replayMessage: Int?, f: () throws -> ReturnType?) -> ReturnType? {
        do {
            return try f()
        } catch {
            bot?.sendMessage(chatID: chatID, text: error.localizedDescription, replayMessage: replayMessage)
            return nil
        }
    }
}
