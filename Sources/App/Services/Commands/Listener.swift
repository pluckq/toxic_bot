//
//  Listener.swift
//  App
//
//  Created by vadim vitvickiy on 01/08/2018.
//

import Vapor
import Telegrammer
import FluentPostgreSQL

class Listener {
    
    var handler: MessageHandler!
    
    private weak var bot: Bot?

    init(bot: Bot) {
        self.bot = bot
        
        handler = MessageHandler(callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let user = update.message?.from,
        let chatID = update.message?.chat.id
            else { return }
        
        if user.id == 89405081 {
            try bot?.sendDocument(params: Bot.SendDocumentParams(chatId: ChatId.chat(chatID), document: FileInfo.fileId("CgADAgAD-QADU5RASiWCgrNZ-ZruAg"), replyToMessageId: update.message?.messageId))
        }
    }
}
