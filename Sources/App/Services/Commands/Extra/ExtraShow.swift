//
//  ExtraShow.swift
//  App
//
//  Created by vadim vitvickiy on 26/07/2018.
//

import Vapor
import Telegrammer
import FluentPostgreSQL

class ExtraShow {
    
    var handler: MessageHandler!
    
    private weak var bot: Bot?

    init(bot: Bot) {
        self.bot = bot
        
        let filter = Filters.regexp(pattern: "\\#(.*)", options: [.caseInsensitive, .useUnicodeWordBoundaries])
        handler = MessageHandler(filters: filter, callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let message = update.message,
            let hashtag = message.findHashtag()
            else { return }
        
        pool
            .requestConnection()
            .flatMap {
                Extra.query(on: $0).filter(\.chatID == message.chat.id).filter(\.hashtag == hashtag).first()
            }.whenSuccess { [unowned self, message] extra in
                if let e = extra {
                    self.sendExtra(e, message: message)
                }
            }
    }
    
    func sendExtra(_ extra: Extra, message: Message) {
        let chatID = message.chat.id
        let replayMessageID = message.replyToMessage?.messageId ?? message.messageId
        
        switch extra.type {
        case .text:
            if let text = extra.text {
                bot?.sendMessage(chatID: message.chat.id, text: text, replayMessage: replayMessageID)
            }
        case .image:
            try? _ = bot?.sendPhoto(params: Bot.SendPhotoParams(chatId: .chat(chatID), photo: FileInfo.fileId(extra.mediaID!), caption: extra.text, replyToMessageId: replayMessageID))
        case .document:
            try? _ = bot?.sendDocument(params: Bot.SendDocumentParams(chatId: .chat(chatID), document: FileInfo.fileId(extra.mediaID!), caption: extra.text, replyToMessageId: replayMessageID))
        case .sticker:
            try? _ = bot?.sendSticker(params: Bot.SendStickerParams(chatId: .chat(chatID), sticker: FileInfo.fileId(extra.mediaID!), replyToMessageId: replayMessageID))
        case .audio:
            try? _ = bot?.sendAudio(params: Bot.SendAudioParams(chatId: .chat(chatID), audio: FileInfo.fileId(extra.mediaID!), caption: extra.text, replyToMessageId: replayMessageID))
        case .video:
            try? _ = bot?.sendVideo(params: Bot.SendVideoParams(chatId: .chat(chatID), video: FileInfo.fileId(extra.mediaID!), caption: extra.text, replyToMessageId: replayMessageID))
        }
    }
}
