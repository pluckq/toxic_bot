//
//  Extra.swift
//  App
//
//  Created by vadim vitvickiy on 23/07/2018.
//

import Vapor
import Telegrammer
import FluentPostgreSQL

class ExtraAdd {
    
    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private let connection = pool.requestConnection()

    init(bot: Bot) {
        self.bot = bot
        handler = CommandHandler(commands: ["/extra"], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let chat = update.message?.chat,
            let replayMessage = update.message?.replyToMessage,
            let hashtag = update.message?.findHashtag()
            else { return }
        
        let chatID = replayMessage.chat.id
        let extra = Extra(message: replayMessage, hachetag: hashtag)
        
        addExtra(chat: chat, extra: extra)
            .whenSuccess { [weak self] chat in
                self?.bot?.sendMessage(chatID: chatID, text: "Content was saved with hashtag \(extra.hashtag)", replayMessage: replayMessage.messageId)
        }
    }
    
    func addExtra(chat: Telegrammer.Chat, extra: Extra) -> Future<Extra> {
        return connection
            .flatMap {
                $0.transaction(on: .psql, { conn in
                    Chat.find(chat.id, on: conn)
                        .flatMap {
                            if $0 == nil {
                                _ = Chat(id: chat.id).create(on: conn)
                            }
                            return extra.create(on: conn)
                    }
                })
        }
    }
}

