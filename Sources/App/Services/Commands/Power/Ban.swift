//
//  Ban.swift
//  App
//
//  Created by vadim vitvickiy on 26/07/2018.
//

import Telegrammer

class Ban {

    var handler: CommandHandler!
    
    private weak var bot: Bot?
    
    init(bot: Bot) {
        self.bot = bot
        handler = CommandHandler(commands: ["/ban"], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let chatID = update.message?.chat.id,
            let messageID = update.message?.replyToMessage?.messageId
            else { return }
        
        try bot?.sendDocument(params: Bot.SendDocumentParams(chatId: ChatId.chat(chatID), document: FileInfo.fileId("CgADAgAD-QADU5RASiWCgrNZ-ZruAg"), replyToMessageId: messageID))
    }
}
