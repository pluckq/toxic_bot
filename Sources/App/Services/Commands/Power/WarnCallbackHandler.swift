//
//  WarnQueryHandler.swift
//  App
//
//  Created by vadim vitvickiy on 27/07/2018.
//

import Telegrammer

class WarnCallbackHandler {

    var handler: CallbackQueryHandler!
    
    init() {
        handler = CallbackQueryHandler(pattern: "\\b(ResetCallback)\\b", callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let user = update.callbackQuery?.message?.replyToMessage?.from
            else { return }
        print(user.firstName)
    }
}
