//
//  SilentBan.swift
//  App
//
//  Created by vadim vitvickiy on 30/07/2018.
//

import Telegrammer

class SilentBan {

    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private weak var rightsValidator: RightsValidator?
    
    init(bot: Bot, rightsValidator: RightsValidator, errorHandler: ErrorHandler) {
        self.bot = bot
        self.rightsValidator = rightsValidator
        self.handler = CommandHandler(commands: ["/silentban"], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let messageID = update.message?.messageId,
            let chatID = update.message?.chat.id,
            let senderID = update.message?.from?.id,
            let userID = update.message?.replyToMessage?.from?.id
            else { return }
        
        rightsValidator?.admins(chatID: chatID).whenSuccess { [weak self] ids in
            if ids.contains(senderID) && !ids.contains(userID) {
                try? self?.bot?.kickChatMember(params: Bot.KickChatMemberParams(chatId: .chat(chatID), userId: userID))
                    .whenSuccess { isBanned in
                        try? _ = self?.bot?.deleteMessage(params: Bot.DeleteMessageParams(chatId: ChatId.chat(chatID), messageId: messageID))
                }
            }
        }
    }
}
