//
//  Promote.swift
//  App
//
//  Created by vadim vitvickiy on 27/07/2018.
//

import Telegrammer

class Promote {
    
    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private weak var rightsValidator: RightsValidator?
    
    init(bot: Bot, rightsValidator: RightsValidator) {
        self.bot = bot
        self.rightsValidator = rightsValidator
//        "/vlast", "/child"
        handler = CommandHandler(commands: [], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let message = update.message,
            let command = message.text,
            let replyMessageID = message.replyToMessage?.messageId,
            let fromUserID = message.from?.id,
            let userID = message.replyToMessage?.from?.id
            else { return }
        let chatID = message.chat.id
        
        var params: Bot.PromoteChatMemberParams
        var answerText: String
        
        if command == "/vlast" {
            params = Bot.PromoteChatMemberParams(chatId: ChatId.chat(chatID), userId: userID, canDeleteMessages: true, canRestrictMembers: true, canPinMessages: true)
            answerText = "User has been promoted."
        } else {
            params = Bot.PromoteChatMemberParams(chatId: ChatId.chat(chatID), userId: userID, canDeleteMessages: false, canRestrictMembers: false, canPinMessages: false)
            answerText = "User has been demoted."
        }
        
        rightsValidator?.admins(chatID: chatID)
            .whenSuccess { [weak self] ids in
                if ids.contains(fromUserID) {
                    try? self?.bot?.promoteChatMember(params: params)
                        .whenSuccess { didPromote in
                            if didPromote {
                                self?.bot?.sendMessage(chatID: chatID, text: answerText, replayMessage: replyMessageID)
                            }
                    }
                }
        }
    }
}
