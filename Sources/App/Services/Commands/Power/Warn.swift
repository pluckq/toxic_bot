//
//  Warn.swift
//  App
//
//  Created by vadim vitvickiy on 26/07/2018.
//

import Telegrammer
import FluentPostgreSQL

class Warn {

    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private weak var rightsValidator: RightsValidator?
    private weak var errorHandler: ErrorHandler?
    
    init(bot: Bot, rightsValidator: RightsValidator, errorHandler: ErrorHandler) {
        self.bot = bot
        self.rightsValidator = rightsValidator
        self.errorHandler = errorHandler
        self.handler = CommandHandler(commands: ["/warn"], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let messageID = update.message?.replyToMessage?.messageId,
            let senderID = update.message?.from?.id,
            let chatID = update.message?.chat.id,
            let userID = update.message?.replyToMessage?.from?.id
            else { return }
        
        try bot?.sendMessage(params: Bot.SendMessageParams(chatId: ChatId.chat(chatID), text: "User has been warned.", replyToMessageId: messageID, replyMarkup: ReplyMarkup.inlineKeyboardMarkup(InlineKeyboardMarkup(inlineKeyboard: [[InlineKeyboardButton(text: "Reset", callbackData: "ResetCallback")]]))))
    }
}
