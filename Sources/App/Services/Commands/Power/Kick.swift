//
//  Kick.swift
//  App
//
//  Created by vadim vitvickiy on 22/07/2018.
//

import Vapor
import Telegrammer

class Kick {
    
    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private weak var rightsValidator: RightsValidator?
    private weak var errorHandler: ErrorHandler?
    
    private var citates = ["Ебаные дети тянутся к кнопкам", "Ты ахуел?", "Пошел нахуй, вахтер", "Жопу себе заварнь", "себя кикни", "о вахтер пришел", "кто дал старику админку?", "Вахта", "/хуярн", "Ни дня без драмы", "Дети плетут интриги", "пизда тупые дети нахуй", "дали им кнопки и жмут все", "На кнопки власти нажимать приятно?", "Что руки чешутся кнопки власти понажимать?", "Глупый ребёнок", "Кто-то жмёт кнопки", "Опять демонстрирует себя как тупого ребенка который при возможности лезет к кнопкам", "ещё один мец", "на кнопки жмёт", "Дети дорвались до кнопок", "Опять дети на кнопки власти кликают", "ебать", "кнопки пошли в ход", "кнопки власти дали петуху"]
    
    init(bot: Bot, rightsValidator: RightsValidator, errorHandler: ErrorHandler) {
        self.bot = bot
        self.rightsValidator = rightsValidator
        self.errorHandler = errorHandler
        self.handler = CommandHandler(commands: ["/kick"], callback: handler)
    }
    
    func handler(update: Update, context: BotContext?) throws {
        guard let messageID = update.message?.messageId,
            let senderID = update.message?.from?.id,
            let chatID = update.message?.chat.id,
            let userID = update.message?.replyToMessage?.from?.id
            else { return }
        
        rightsValidator?.admins(chatID: chatID).whenSuccess { [weak self] ids in
            if ids.contains(senderID) && !ids.contains(userID) {
                self?.errorHandler?.wrap(chatID: chatID, replayMessage: messageID) {
                    try self?.bot?.kickChatMember(params: Bot.KickChatMemberParams(chatId: .chat(chatID), userId: userID))
                        .whenSuccess{ isBanned in
                            if isBanned {
                                try? _ = self?.bot?.unbanChatMember(params: Bot.UnbanChatMemberParams(chatId: .chat(chatID), userId: userID))
                            }
                        }
                }
            } else {
                self?.bot?.sendMessage(chatID: chatID, text: self?.citates.random ?? "", replayMessage: messageID)
            }
        }
    }
}
