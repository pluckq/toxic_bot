//
//  CommandProtocol.swift
//  App
//
//  Created by vadim vitvickiy on 03/07/2018.
//

import Vapor
import Telegrammer

protocol CommandProtocol {
    
    var bot: Bot? { get set }
    var handler: CommandHandler { get set }
}
