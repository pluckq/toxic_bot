//
//  Vadik.swift
//  App
//
//  Created by vadim vitvickiy on 02/07/2018.
//

import Vapor
import Telegrammer

final class Vadik {
    
    var handler: CommandHandler!
    
    private weak var bot: Bot?
    private weak var rightsValidator: RightsValidator?
    private weak var errorHandler: ErrorHandler?
    
    init(bot: Bot, rightsValidator: RightsValidator, errorHandler: ErrorHandler) {
        self.bot = bot
        self.rightsValidator = rightsValidator
        self.errorHandler = errorHandler
        self.handler = CommandHandler(commands: ["/vadik"], callback: handler)
    }

    func handler(update: Update, context: BotContext?) throws {
        guard let messageID = update.message?.messageId,
            let chatID = update.message?.chat.id,
            let userID = update.message?.from?.id
            else { return }
        
        rightsValidator?.admins(chatID: chatID).whenSuccess { [weak self] ids in
            if ids.contains(userID) {
                try? _ = self?.bot?.sendMessage(params: Bot.SendMessageParams(chatId: .chat(chatID), text: "Нельзя кикнуть пидора с админкой", replyToMessageId: messageID))
            } else {
                self?.errorHandler?.wrap(chatID: chatID, replayMessage: messageID, f: {
                    try self?.bot?.kickChatMember(params: Bot.KickChatMemberParams(chatId: .chat(chatID), userId: userID))
                        .whenSuccess { isBanned in
                            if isBanned {
                                try? _ = self?.bot?.unbanChatMember(params: Bot.UnbanChatMemberParams(chatId: .chat(chatID), userId: userID))
                            }
                        }
                })
            }
        }
    }
}
